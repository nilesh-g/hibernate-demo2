package com.sunbeaminfo.sh.hb;

import org.hibernate.Session;

public class Hb02Main {
	public static void main(String[] args) {
		try(Session session = HbUtil.openSession()) {
			Book b = session.get(Book.class, 11);
			System.out.println("Found : " + b);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
