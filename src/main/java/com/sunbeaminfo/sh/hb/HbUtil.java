package com.sunbeaminfo.sh.hb;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HbUtil {
	private static SessionFactory factory = createSessionFactory();
	
	private static SessionFactory createSessionFactory() {
		Configuration cfg = new Configuration(); // empty config obj
		cfg.configure(); // read hibernate.cfg.xml
		return cfg.buildSessionFactory(); // create factory
	}
	
	public static SessionFactory getSessionFactory() {
		return factory;
	}
	
	public static Session openSession() {
		return factory.openSession();
	}
	
	public static void shutdown() {
		if(factory!=null)
			factory.close();
	}
}
